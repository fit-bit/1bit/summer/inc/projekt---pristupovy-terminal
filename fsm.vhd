-- fsm.vhd: Finite State Machine
-- Author(s): Mat�j Kudera (xkuder04)
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (TEST1, TEST2, TEST3, TEST4, TEST5, TEST6, TEST7, TEST8, TEST9, TEST10, TEST11, TEST12, TEST13, TEST14, TEST15, TEST16, TEST17, TEST18, TEST19, TEST20, WRONG, PRINT_ERR, PRINT_OK, FINISH);
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= TEST1;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
	-- KEY(0) = 0
	-- KEY(1) = 1
	-- KEY(2) = 2
	-- KEY(3) = 3
	-- KEY(4) = 4
	-- KEY(5) = 5
	-- KEY(6) = 6
	-- KEY(7) = 7
	-- KEY(8) = 8
	-- KEY(9) = 9
	-- KEY(10) = A
	-- KEY(11) = B
	-- KEY(12) = C
	-- KEY(13) = D
	-- KEY(14) = *
	-- KEY(15) = #
   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST1 => -- KEY=3
      next_state <= TEST1;
      if (KEY(3) = '1') then
         next_state <= TEST2;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST2 => -- KEY=7 mebo KEY=4
      next_state <= TEST2;
      if (KEY(7) = '1') then
         next_state <= TEST3; 
      elsif (KEY(4) = '1') then
         next_state <= TEST12; 
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
       elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST3 => -- KEY=2
      next_state <= TEST3;
      if (KEY(2) = '1') then
         next_state <= TEST4;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST4 => -- KEY=2
      next_state <= TEST4;
      if (KEY(2) = '1') then
         next_state <= TEST5;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST5 => -- KEY=6
      next_state <= TEST5;
      if (KEY(6) = '1') then
         next_state <= TEST6;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST6 => -- KEY=3
      next_state <= TEST6;
      if (KEY(3) = '1') then
         next_state <= TEST7;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST7 => -- KEY=7
      next_state <= TEST7;
      if (KEY(7) = '1') then
         next_state <= TEST8;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST8 => -- KEY=0
      next_state <= TEST8;
      if (KEY(0) = '1') then
         next_state <= TEST9;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST9 => -- KEY=3
      next_state <= TEST9;
      if (KEY(3) = '1') then
         next_state <= TEST10;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST10 => -- KEY=6
      next_state <= TEST10;
      if (KEY(6) = '1') then
         next_state <= TEST11;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST11 => -- K�d je OK, te� se mus� d�t # jinak WRONG
      next_state <= TEST11;
      if (KEY(15) = '1') then
         next_state <= PRINT_OK;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST12 => -- KEY=8
      next_state <= TEST12;
      if (KEY(8) = '1') then
         next_state <= TEST13;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST13 => -- KEY=9
      next_state <= TEST13;
      if (KEY(9) = '1') then
         next_state <= TEST14;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST14 => -- KEY=0
      next_state <= TEST14;
      if (KEY(0) = '1') then
         next_state <= TEST15;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST15 => -- KEY=5
      next_state <= TEST15;
      if (KEY(5) = '1') then
         next_state <= TEST16;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST16 => -- KEY=4
      next_state <= TEST16;
      if (KEY(4) = '1') then
         next_state <= TEST17;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST17 => -- KEY=8
      next_state <= TEST17;
      if (KEY(8) = '1') then
         next_state <= TEST18;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST18 => -- KEY=1
      next_state <= TEST18;
      if (KEY(1) = '1') then
         next_state <= TEST19;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST19 => -- KEY=4
      next_state <= TEST19;
      if (KEY(4) = '1') then
         next_state <= TEST20;
      elsif (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST20 => -- K�d je OK, te� se mus� d�t # jinak WRONG
      next_state <= TEST20;
      if (KEY(15) = '1') then
	 next_state <= PRINT_OK;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_OK =>
      next_state <= PRINT_OK;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_ERR =>
      next_state <= PRINT_ERR;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when WRONG => -- Je zad�n �patn� k�d, ale za��zen� to nad�v� najevo dokud se nezm��kne #
      next_state <= WRONG;
      if (KEY(15) = '1') then
	 next_state <= PRINT_ERR;
      elsif (KEY(14 downto 0) /= "000000000000000") then
	 next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when FINISH => -- Vyma�e zpr�vu na obrzovce a jede se odznova 
      next_state <= FINISH;
      if (KEY(15) = '1') then
         next_state <= TEST1; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
      next_state <= TEST1;
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_OK =>
      FSM_CNT_CE     <= '1';
      FSM_MX_MEM     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_ERR =>
      FSM_CNT_CE     <= '1';
      -- Pam� je nastaven� u� na za��tku na 0
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - -
   when FINISH =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others => -- Plat� pro v�echny testy a WRONG
      if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   end case;
end process output_logic;

end architecture behavioral;

